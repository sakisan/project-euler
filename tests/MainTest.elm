module MainTest exposing (suite)

import Expect
import P_0_30 exposing (..)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "project euler"
        -- [ test "problem 1" <| \_ -> Expect.equal 23 (p1 10)
        -- [ test "problem 2" <| \_ -> Expect.equal (2 + 8 + 34) (p2 90)
        -- [ test "problem 3" <| \_ -> Expect.equal 29 (p3 13195)
        -- [ test "problem 4" <| \_ -> Expect.equal 9009 (p4 2)
        -- [ test "problem 5" <| \_ -> Expect.equal 2520 (p5 1 10)
        -- [ test "problem 6" <| \_ -> Expect.equal 2640 (p6 1 10)
        -- [ test "problem 7" <| \_ -> Expect.equal 13 (p7 6)
        -- [ test "problem 8" <| \_ -> Expect.equal 5832 (p8 4)
        -- [ test "problem 9" <| \_ -> Expect.equal 31875000 (p9 ())
        -- [ test "problem 10" <| \_ -> Expect.equal 17 (p10 10)
        -- [ test "problem 11" <| \_ -> Expect.equal 99 (p11 1)
        -- [ test "problem 12" <| \_ -> Expect.equal 28 (p12 5)
        -- [ test "problem 13" <| \_ -> Expect.equal "5537376230" (p13 ())
        -- [ test "problem 14" <| \_ -> Expect.equal 9 (p14 10)
        -- [ test "problem 15" <| \_ -> Expect.equal 6 (p15 ( 2, 2 ))
        -- [ test "problem 16" <| \_ -> Expect.equal 26 (p16 15)
        -- [ test "problem 18" <| \_ -> Expect.equal 23 (p18 "3\n7 4\n 2 4 6\n 8 5 9 3")
        -- [ test "problem 19" <| \_ -> Expect.equal 171 (p19 ())
        -- [ test "problem 20" <| \_ -> Expect.equal 27 (p20 10)
        -- [ test "problem 21" <| \_ -> Expect.equal 31626 (p21 ())
        -- [ test "problem 22" <| \_ -> Expect.equal 211 (p22 "\"MARY\",\"PATRICIA\"")
        -- [ test "problem 23" <| \_ -> Expect.equal 4179871 (p23 ())
        -- [ test "problem 24" <| \_ -> Expect.equal "102" (p24 3 [0, 1, 2])
        -- [ test "problem 25" <| \_ -> Expect.equal 12 (p25 3)
        -- [ test "problem 27" <| \_ -> Expect.equal -59231 (p27 999 1000)
        [ test "problem 29" <| \_ -> Expect.equal 15 (p29 5 5)
        -- [ test "problem 30" <| \_ -> Expect.equal 19316 (p30 4)
        ]

module FibonacciTest exposing (suite)

import Expect
import Fibonacci exposing (..)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Fibonacci"
        [ test "sequence" <|
            \_ -> Expect.equal [ 1, 2, 3, 5, 8 ] (sequence (\v -> v < 10))
        ]

module TriangleNumberTest exposing (suite)

import Expect
import TriangleNumber exposing (..)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "TriangleNumber"
        [ test "triangleNumber" <|
            \_ -> Expect.equal 28 (triangleNumber 7)
        ]

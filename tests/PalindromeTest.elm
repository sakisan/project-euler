module PalindromeTest exposing (suite)

import Expect
import Palindrome exposing (..)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Palindrome"
        [ test "isPalindrome true" <|
            \_ -> Expect.equal True (isPalindrome "racecar")
        , test "isPalindrome false" <|
            \_ -> Expect.equal False (isPalindrome "desserts")
        , test "isIntPalindrome true" <|
            \_ -> Expect.equal True (isIntPalindrome 9009)
        , test "isIntPalindrome false" <|
            \_ -> Expect.equal False (isIntPalindrome 9909)
        ]

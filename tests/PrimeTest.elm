module PrimeTest exposing (suite)

import Expect
import Prime exposing (..)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Prime"
        [ test "factors" <|
            \_ -> Expect.equal [ 1, 2, 3, 6 ] (factors 6)
        , test "factors of square number" <|
            \_ -> Expect.equal [ 1, 5, 25 ] (factors 25)
        , test "primeFactors" <|
            \_ -> Expect.equal [ 5, 7, 13, 29 ] (primeFactors 13195)
        , test "isPrime 2" <|
            \_ -> Expect.equal True (isPrime 2)
        , test "isPrime 3" <|
            \_ -> Expect.equal True (isPrime 3)
        , test "isPrime 29" <|
            \_ -> Expect.equal True (isPrime 29)
        , test "isPrime 4" <|
            \_ -> Expect.equal False (isPrime 4)
        , test "isPrime 111" <|
            \_ -> Expect.equal False (isPrime 111)
        , test "primesUnder 10" <|
            \_ -> Expect.equal [ 2, 3, 5, 7 ] (primesUnder 10)
        ]

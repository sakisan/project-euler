module ConvertTest exposing (suite)

import Convert exposing (..)
import Expect
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "Convert"
        [ test "charToInt" <|
            \_ -> Expect.equal 0 (charToInt '0')
        ]

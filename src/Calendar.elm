module Calendar exposing (compareDate, compareMonths, daysTo, fromMonthOrdinal, fromWeekdayOrdinal, getWeekday, isLeapYear, monthOrdinal, nextDate, nextMonth, numberOfDays, numberOfDaysInYear, weekdayOrdinal)

import Exts.List as ExtsList
import Time exposing (Month(..), Weekday(..))
import Utils


isLeapYear : Int -> Bool
isLeapYear year =
    if Utils.divisibleBy 400 year then
        True

    else if Utils.divisibleBy 100 year then
        False

    else if Utils.divisibleBy 4 year then
        True

    else
        False


numberOfDays : Int -> Month -> Int
numberOfDays year month =
    case month of
        Jan ->
            31

        Feb ->
            if isLeapYear year then
                29

            else
                28

        Mar ->
            31

        Apr ->
            30

        May ->
            31

        Jun ->
            30

        Jul ->
            31

        Aug ->
            31

        Sep ->
            30

        Oct ->
            31

        Nov ->
            30

        Dec ->
            31


numberOfDaysInYear : Int -> Int
numberOfDaysInYear year =
    if isLeapYear year then
        366

    else
        365


getWeekday : Int -> Month -> Int -> Maybe Weekday
getWeekday year month day =
    if day > numberOfDays year month then
        Nothing

    else
        daysTo ( 1900, Jan, 1 ) ( year, month, day )
            |> remainderBy 7
            |> fromWeekdayOrdinal
            |> Just


weekdayOrdinal : Weekday -> Int
weekdayOrdinal day =
    case day of
        Mon ->
            0

        Tue ->
            1

        Wed ->
            2

        Thu ->
            3

        Fri ->
            4

        Sat ->
            5

        Sun ->
            6


fromWeekdayOrdinal : Int -> Weekday
fromWeekdayOrdinal day =
    case remainderBy 7 day of
        0 ->
            Mon

        1 ->
            Tue

        2 ->
            Wed

        3 ->
            Thu

        4 ->
            Fri

        5 ->
            Sat

        6 ->
            Sun

        _ ->
            Mon


monthOrdinal : Month -> Int
monthOrdinal m =
    case m of
        Jan ->
            0

        Feb ->
            1

        Mar ->
            2

        Apr ->
            3

        May ->
            4

        Jun ->
            5

        Jul ->
            6

        Aug ->
            7

        Sep ->
            8

        Oct ->
            9

        Nov ->
            10

        Dec ->
            11


fromMonthOrdinal : Int -> Month
fromMonthOrdinal n =
    case remainderBy 12 n of
        0 ->
            Jan

        1 ->
            Feb

        2 ->
            Mar

        3 ->
            Apr

        4 ->
            May

        5 ->
            Jun

        6 ->
            Jul

        7 ->
            Aug

        8 ->
            Sep

        9 ->
            Oct

        10 ->
            Nov

        11 ->
            Dec

        _ ->
            Jan


compareMonths : Month -> Month -> Order
compareMonths a b =
    compare (monthOrdinal a) (monthOrdinal b)


nextMonth : Month -> Month
nextMonth month =
    fromMonthOrdinal (monthOrdinal month + 1)


compareDate : ( Int, Month, Int ) -> ( Int, Month, Int ) -> Order
compareDate ( year1, month1, day1 ) ( year2, month2, day2 ) =
    case compare year1 year2 of
        EQ ->
            case compareMonths month1 month2 of
                EQ ->
                    case compare day1 day2 of
                        EQ ->
                            EQ

                        GT ->
                            GT

                        LT ->
                            LT

                GT ->
                    GT

                LT ->
                    LT

        GT ->
            GT

        LT ->
            LT


daysTo : ( Int, Month, Int ) -> ( Int, Month, Int ) -> Int
daysTo (( year1, month1, day1 ) as date1) (( year2, month2, day2 ) as date2) =
    let
        ( dateA, dateB ) =
            case compareDate date1 date2 of
                EQ ->
                    ( date1, date2 )

                GT ->
                    ( date2, date1 )

                LT ->
                    ( date1, date2 )

        helper date count =
            if compareDate date dateB == LT then
                helper (nextDate date) (count + 1)

            else
                count
    in
    helper dateA 0


nextDate : ( Int, Month, Int ) -> ( Int, Month, Int )
nextDate ( year, month, day ) =
    if day == numberOfDays year month then
        if month == Dec then
            ( year + 1, Jan, 1 )

        else
            ( year, nextMonth month, 1 )

    else
        ( year, month, day + 1 )

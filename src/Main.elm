module Main exposing (..)

import Array exposing (Array)
import BigInt exposing (BigInt)
import Calendar
import Coins
import Convert
import Dict exposing (Dict)
import Exts.List as ExtsList
import Fibonacci
import Html
import List.Extra as ListExtra
import Maybe.Extra as MaybeExtra
import Palindrome
import Parser exposing ((|.), (|=), Parser)
import Prime
import Set
import Time exposing (Month(..), Weekday(..))
import TriangleNumber
import Utils


main =
    Html.text ""


p31 : () -> Int
p31 _ =
    -- Coin sums
    Coins.numberOfCombinations [ 1, 2, 5, 10, 20, 50, 100, 200 ] 200


p32 : () -> Int
p32 _ =
    -- Pandigital products
    let
        getProduct : List Int -> Maybe Int
        getProduct permutation =
            case permutation of
                [ a, b, c, d, e, f, g, h, i ] ->
                    if
                        (((10 * a + b) * (100 * c + 10 * d + e))
                            == (1000 * f + 100 * g + 10 * h + i)
                        )
                            || ((a * (1000 * b + 100 * c + 10 * d + e))
                                    == (1000 * f + 100 * g + 10 * h + i)
                               )
                    then
                        -- let _ = Debug.log "" permutation in
                        Just (1000 * f + 100 * g + 10 * h + i)

                    else
                        Nothing

                _ ->
                    Nothing
    in
    ListExtra.permutations [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
        |> List.filterMap getProduct
        |> Set.fromList
        |> Set.toList
        -- |> Debug.log ""
        |> List.sum


p33 : () -> Int
p33 _ =
    -- Digit cancelling fractions
    let
        twoDigitNumbers =
            List.range 10 99

        isNonTrivial a b =
            not (Utils.divisibleBy 10 a && Utils.divisibleBy 10 b)

        areCancelling a b =
            let
                target =
                    toFloat a / toFloat b

                ( a1, a2 ) =
                    ( toFloat <| a // 10, toFloat <| remainderBy 10 a )

                ( b1, b2 ) =
                    ( toFloat <| b // 10, toFloat <| remainderBy 10 b )
            in
            (a2 == b2 && a1 / b1 == target)
                || (a1 == b2 && a2 / b1 == target)
                || (a2 == b1 && a1 / b2 == target)
                || (a1 == b1 && a2 / b2 == target)

        ( nominator, denominator ) =
            Utils.cartesianProduct twoDigitNumbers twoDigitNumbers
                |> List.filter (\( a, b ) -> a < b && isNonTrivial a b && areCancelling a b)
                |> Debug.log ""
                |> List.foldl (\( a, b ) ( x, z ) -> ( a * x, b * z )) ( 1, 1 )
                |> Debug.log ""

        ( nfactors, dfactors ) =
            ( Prime.primeFactors nominator, Prime.primeFactors denominator )
                |> Debug.log ""
    in
    List.foldl ListExtra.remove dfactors nfactors
        |> Debug.log ""
        |> List.product


p34 : () -> Int
p34 _ =
    -- Digit factorials
    let
        factorial n =
            if n <= 0 then
                1

            else
                n * factorial (n - 1)

        test num =
            (Convert.digits num
                |> List.map factorial
                |> List.sum
            )
                == num
    in
    List.range 3 9999999
        |> List.filter test
        |> List.sum


p35 : Int -> Int
p35 limit =
    -- Circular primes
    let
        primes =
            Prime.primesUnder limit

        isPrime a =
            List.member a primes

        rotate : Int -> List Int -> List Int
        rotate i list =
            List.drop i list ++ List.take i list

        rotations : List Int -> List (List Int)
        rotations list =
            List.range 0 (List.length list - 1)
                |> List.map (\i -> rotate i list)

        isCircularPrime num =
            Convert.digits num
                |> rotations
                |> List.map Convert.concatDigits
                |> List.all isPrime
    in
    primes
        |> List.filter isCircularPrime
        -- |> Debug.log ""
        |> List.length


p36 : Int -> Int
p36 limit =
    -- Double-base palindromes
    List.range 0 limit
        |> List.filter
            (\i ->
                Palindrome.isIntPalindrome i
                    && (Convert.toBinary i
                            |> List.map String.fromInt
                            |> String.concat
                            |> Palindrome.isPalindrome
                       )
            )
        -- |> Debug.log ""
        |> List.sum


p37 : Int -> Int
p37 limit =
    -- Truncatable primes
    let
        primes =
            Prime.primesUnder limit

        isPrime a =
            List.member a primes

        isTruncatable num =
            let
                digits =
                    Convert.digits num

                left =
                    ListExtra.inits digits

                right =
                    ListExtra.tails digits

                test list =
                    List.filter (not << List.isEmpty) list
                        |> List.map Convert.concatDigits
                        |> List.all isPrime
            in
            test left && test right
    in
    primes
        |> List.filter (\p -> p >= 10)
        |> List.filter isTruncatable
        |> Debug.log ""
        |> List.sum


p38 _ =
    -- Pandigital multiples
    let
        digits n base =
            List.range 1 n
                |> List.map (\c -> String.fromInt (base * c))
                |> String.concat
                |> String.toList
                |> List.map Convert.charToInt

        isPandigital ds =
            List.range 1 9
                |> List.all (\d -> List.member d ds)
    in
    List.range 9000 9999
        |> List.filter (\n -> isPandigital (digits 2 n))
        |> List.map (digits 2)
        |> List.map Convert.concatDigits


p39 : () -> Int
p39 _ =
    -- Integer right triangles
    let
        getHypotenuse : Float -> Float -> Float
        getHypotenuse perimeter side =
            -- side ^ 2 + otherside ^ 2 = hypotenuse ^ 2
            -- otherside ^ 2 = hypotenuse ^ 2 - side ^ 2
            -- otherside = sqrt (hypotenuse ^ 2 - side ^ 2)
            --
            -- side + otherside + hypotenuse = perimeter
            -- otherside = perimeter - side - hypotenuse
            --
            -- perimeter - side - hypotenuse = sqrt (hypotenuse ^ 2 - side ^ 2)
            -- (perimeter - side - hypotenuse) ^ 2 = hypotenuse ^ 2 - side ^ 2
            -- perimeter - side = r (is known)
            -- (r - hypotenuse) ^ 2 = hypotenuse ^ 2 - side ^ 2
            -- r ^ 2 - 2 * remainder * hypotenuse + hypotenuse ^ 2 = hypotenuse ^ 2 - side ^ 2
            -- 2 * remainder * hypotenuse = side ^ 2 + r ^ 2
            -- hypotenuse = (side ^ 2 + (perimeter - side) ^ 2) / 2 * (perimeter - side)
            (side ^ 2 + (perimeter - side) ^ 2) / (2 * (perimeter - side))

        getSolutions : Float -> Float -> List Int
        getSolutions perimeter side =
            let
                hypotenuse =
                    getHypotenuse perimeter side
            in
            if toFloat (round hypotenuse) == hypotenuse then
                [ round side, round hypotenuse, round (perimeter - hypotenuse) ]

            else
                []

        numberOfSolutions p =
            List.range 1 (p // 4)
                |> List.map (\side -> getSolutions (toFloat p) (toFloat side))
                |> List.concat
                |> List.length
    in
    List.range 1 1000
        |> ExtsList.maximumBy numberOfSolutions
        |> Utils.orCrash ""


p40 : () -> Int
p40 _ =
    -- Champernowne's constant
    let
        next : Int -> Int -> Int
        next nth i =
            let
                -- _ = Debug.log "" ( nth, i )
                numberOfDigitsInI =
                    Convert.numberOfDigits i
            in
            if numberOfDigitsInI >= nth then
                remainderBy 10 (i // (10 ^ (numberOfDigitsInI - nth)))

            else
                next (nth - numberOfDigitsInI) (i + 1)

        digit d =
            next d 1
                |> Debug.log ""
    in
    digit 1
        * digit 10
        * digit 100
        * digit 1000
        * digit 10000
        * digit 100000
        * digit 1000000


p41 : () -> Int
p41 _ =
    -- Pandigital prime
    let
        -- primes = Prime.primesUnder (10 ^ 9)
        -- this crashes on memory, so I tried something else
        try : Int -> Maybe Int
        try max =
            ListExtra.permutations (List.range 1 max)
                |> List.map Convert.concatDigits
                |> List.filter Prime.isPrime
                |> List.maximum

        decrementing max =
            if max == 0 then
                0

            else
                case try max of
                    Just a ->
                        a

                    Nothing ->
                        decrementing (max - 1)
    in
    decrementing 9


p42 : List String -> Int
p42 words =
    -- Coded triangle numbers
    let
        alphabeticalPosition char =
            Convert.charToInt char - 16

        wordValue word =
            String.toList word
                |> List.map alphabeticalPosition
                |> List.sum

        isTriangleWord word =
            TriangleNumber.isTriangleNumber (wordValue word)
    in
    words
        |> List.filter isTriangleWord
        |> List.length


p43 : () -> Int
p43 _ =
    -- Sub-string divisibility
    let
        helper a b c prime =
            Utils.divisibleBy prime (Convert.concatDigits [ a, b, c ])

        testSingle digits =
            case digits of
                [ d1, d2, d3, d4, d5, d6, d7, d8, d9, d10 ] ->
                    helper d2 d3 d4 2
                        && helper d3 d4 d5 3
                        && helper d4 d5 d6 5
                        && helper d5 d6 d7 7
                        && helper d6 d7 d8 11
                        && helper d7 d8 d9 13
                        && helper d8 d9 d10 17

                _ ->
                    False

        testV digits =
            if testSingle digits then
                Just digits

            else
                Nothing

        test digits =
            case digits of
                [ d1, d2, d3, d4, d5, d6, d7, d8, d9 ] ->
                    [ testV [ d1, d2, d3, d4, d5, d6, d7, d8, d9, 9 ]
                    , testV [ d1, d2, d3, d4, d5, d6, d7, d8, 9, d9 ]
                    , testV [ d1, d2, d3, d4, d5, d6, d7, 9, d8, d9 ]
                    , testV [ d1, d2, d3, d4, d5, d6, 9, d7, d8, d9 ]
                    , testV [ d1, d2, d3, d4, d5, 9, d6, d7, d8, d9 ]
                    , testV [ d1, d2, d3, d4, 9, d5, d6, d7, d8, d9 ]
                    , testV [ d1, d2, d3, 9, d4, d5, d6, d7, d8, d9 ]
                    , testV [ d1, d2, 9, d3, d4, d5, d6, d7, d8, d9 ]
                    , testV [ d1, 9, d2, d3, d4, d5, d6, d7, d8, d9 ]
                    , testV [ 9, d1, d2, d3, d4, d5, d6, d7, d8, d9 ]
                    ]
                        |> List.filterMap identity

                _ ->
                    []
    in
    -- permutations of (List.range 0 9) is too big
    List.range 0 8
        |> ListExtra.permutations
        |> List.map test
        |> List.filter (not << List.isEmpty)
        |> List.concat
        |> Set.fromList
        |> Set.toList
        |> List.map Convert.concatDigits
        |> Debug.log ""
        |> List.sum


p44 : () -> Int
p44 _ =
    -- Pentagon numbers
    let
        p : Int -> Int
        p =
            TriangleNumber.pentagonalNumber

        isP : Int -> Bool
        isP =
            TriangleNumber.isPentagonal

        d : Int -> Int -> Maybe Int
        d p1 p2 =
            if isP (p1 + p2) && isP (abs (p1 - p2)) then
                -- if isP (p1 + p2) then
                -- if isP (abs (p1 - p2)) then
                Just (abs (p1 - p2))

            else
                Nothing

        try : Int -> Int -> Int -> Int
        try p1 p2 dd =
            -- let _ = Debug.log "trying" (p1, p2, dd) in
            case d (p p1) (p p2) of
                Just distance ->
                    let
                        _ =
                            Debug.log "" ( p1, p2 )
                    in
                    let
                        _ =
                            Debug.log "" ( p p1, p p2 )
                    in
                    distance

                Nothing ->
                    if dd > 10000 then
                        -1

                    else if p1 > 10000 then
                        try 1 (p2 + dd) (dd + 1)

                    else if p2 - p1 > dd then
                        try (p1 + 1) (p1 + 2) dd

                    else
                        try p1 (p2 + 1) dd
    in
    -- this isn't guaranteed to find the minimised D
    -- however at least it does a fair attempt at iterating in a smart way
    -- and it got lucky to find the right answer ^^
    try 1 2 1


p45 : Int -> Int
p45 lowerBound =
    -- Triangular, pentagonal, and hexagonal
    TriangleNumber.findFirstTriangleNumberFor
        (\n ->
            n
                > lowerBound
                && TriangleNumber.isTriangleNumber n
                && TriangleNumber.isPentagonal n
                && TriangleNumber.isHexagonal n
        )


p46 : () -> Int
p46 _ =
    -- Goldbach's other conjecture
    let
        nextOddCompositeNumber : Int -> Int
        nextOddCompositeNumber min =
            if Utils.isOdd min && Prime.isComposite min then
                min

            else
                nextOddCompositeNumber (min + 2)

        conjecture : List Int -> Int -> Bool
        conjecture primeList num =
            case primeList of
                [] ->
                    False

                prime :: morePrimes ->
                    if prime > num then
                        conjecture morePrimes num

                    else if Utils.isSquare ((num - prime) // 2) then
                        True

                    else
                        conjecture morePrimes num

        test : List Int -> Int -> Int
        test primes start =
            let
                _ =
                    if Utils.divisibleBy 10000 (start - 1) then
                        Debug.log "" start

                    else
                        start

                updatedPrimes : List Int
                updatedPrimes =
                    Prime.incrementalPrimeList primes next

                next : Int
                next =
                    nextOddCompositeNumber start
            in
            if conjecture updatedPrimes next then
                test updatedPrimes (next + 2)

            else
                next
    in
    -- make sure to use an odd number
    test [] 1


p47 : Int -> Int
p47 a =
    -- Distinct primes factors
    let
        primesD =
            Prime.primeFactorsSet (a + 3)
    in
    if List.length primesD /= 4 then
        p47 (a + 4)

    else
        let
            primesC =
                Prime.primeFactorsSet (a + 2)
        in
        if List.length primesC /= 4 then
            p47 (a + 3)

        else
            let
                primesB =
                    Prime.primeFactorsSet (a + 1)
            in
            if List.length primesB /= 4 then
                p47 (a + 2)

            else
                let
                    primesA =
                        Prime.primeFactorsSet a
                in
                if List.length primesA /= 4 then
                    p47 (a + 1)

                else
                    -- let
                    --     checkDistinctPrimes : List (List Int) -> Bool
                    --     checkDistinctPrimes =
                    --         List.concat >> Set.fromList >> Set.size >> (==) 16
                    -- in
                    -- if checkDistinctPrimes [ primesA, primesB, primesC, primesD ] then
                    --     a
                    -- else
                    --     p47 (a + 1)
                    a


p48 : Int -> Int -> Int
p48 n a =
    -- Self powers
    let
        newN =
            truncate (n + truncatedSelfPower a a)

        truncate =
            remainderBy (10 ^ 10)

        truncatedSelfPower i j =
            if j == 1 then
                truncate i

            else
                truncatedSelfPower (truncate (i * a)) (j - 1)
    in
    if a == 1000 then
        newN

    else
        p48 newN (a + 1)


p49 : () -> Int
p49 _ =
    -- Prime permutations
    let
        read : Int -> Dict String (List Int) -> Dict String (List Int)
        read prime dict =
            Dict.update (key prime)
                (\maybe ->
                    case maybe of
                        Nothing ->
                            Just [ prime ]

                        Just list ->
                            Just (prime :: list)
                )
                dict

        key : Int -> String
        key =
            String.fromInt >> String.toList >> List.sort >> String.fromList

        testPermutations : List Int -> List (List Int)
        testPermutations list =
            case list of
                a :: b :: c :: xs ->
                    case testPermutationsFor a (b :: c :: xs) of
                        Nothing ->
                            testPermutations (b :: c :: xs)

                        Just found ->
                            [ found ] ++ testPermutations (b :: c :: xs)

                _ ->
                    []

        testPermutationsFor : Int -> List Int -> Maybe (List Int)
        testPermutationsFor a list =
            case list of
                b :: xs ->
                    if List.member (b + (b - a)) xs then
                        Just [ a, b, b + b - a ]

                    else
                        testPermutationsFor a xs

                _ ->
                    Nothing
    in
    Prime.primesUnder 10000
        |> List.filter (\p -> p >= 1000)
        |> List.foldl read Dict.empty
        |> Dict.values
        |> List.filter (\list -> List.length list >= 3)
        |> List.map List.sort
        |> List.map testPermutations
        |> List.filter (not << List.isEmpty)
        |> Debug.log ""
        |> List.length



{- <leader>aa

   elm repl
   import Main exposing (..)

   import Main exposing (..)

   p49 ()

-}

module Convert exposing (..)

import Array exposing (Array)
import Maybe.Extra as MaybeExtra
import Utils


charToInt : Char -> Int
charToInt char =
    Char.toCode char - 48


toInt : String -> Int
toInt =
    String.toInt >> Maybe.withDefault -1000000


allDigits : String -> List Int
allDigits =
    String.toList >> List.filter Char.isDigit >> List.map charToInt


digits : Int -> List Int
digits n =
    let
        helper : List Int -> Int -> List Int
        helper list num =
            if num < 10 then
                num :: list

            else
                helper (remainderBy 10 num :: list) (num // 10)
    in
    helper [] n


concatDigits : List Int -> Int
concatDigits =
    List.foldl (\d sum -> 10 * sum + d) 0


numberGrid : String -> Array (Array Int)
numberGrid input =
    String.lines input
        |> List.filter (\line -> String.length line > 0)
        |> List.map (String.words >> List.map toInt >> Array.fromList)
        |> Array.fromList


numberColumn : String -> List Int
numberColumn input =
    numberGrid input
        |> Array.map (Array.get 0)
        |> Array.toList
        |> MaybeExtra.values


toBinary : Int -> List Int
toBinary i =
    let
        helper : List Int -> Int -> List Int
        helper prev j =
            if j <= 1 then
                j :: prev

            else if remainderBy 2 j == 0 then
                helper (0 :: prev) (j // 2)

            else
                helper (1 :: prev) (j // 2)
    in
    helper [] i


numberOfDigits : Int -> Int
numberOfDigits i =
    ceiling (logBase 10 (toFloat (i + 1)))

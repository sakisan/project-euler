module P_0_30 exposing (..)

import Array exposing (Array)
import BigInt exposing (BigInt)
import Calendar
import Coins
import Convert
import Dict exposing (Dict)
import Exts.List as ExtsList
import Fibonacci
import Html
import List.Extra as ListExtra
import Maybe.Extra as MaybeExtra
import Palindrome
import Parser exposing ((|.), (|=), Parser)
import Prime
import Set
import Time exposing (Month(..), Weekday(..))
import TriangleNumber
import Utils


main =
    p1
        |> Debug.toString
        |> Html.text


p1 : Int -> Int
p1 cap =
    -- Multiples of 3 and 5
    List.range 0 (cap - 1)
        |> List.filter (\n -> remainderBy 3 n == 0 || remainderBy 5 n == 0)
        |> List.sum


p2 : Int -> Int
p2 cap =
    -- Even Fibonacci numbers
    Fibonacci.sequence (\v -> v < cap)
        |> List.filter (\v -> remainderBy 2 v == 0)
        |> List.sum


p3 : Int -> Int
p3 input =
    -- Largest prime factor
    Prime.primeFactorsSet input
        |> List.maximum
        |> Maybe.withDefault input


p4 : Int -> Int
p4 digits =
    -- Largest palindrome product
    let
        max =
            (10 ^ digits) - 1

        min =
            10 ^ (digits - 1)

        highest =
            max * max

        isProduct n v =
            if remainderBy v n == 0 && n // v < max && n // v > min then
                True

            else if v == min then
                False

            else
                isProduct n (v - 1)

        helper n =
            if isProduct n max && Palindrome.isIntPalindrome n then
                n

            else
                helper (n - 1)
    in
    helper highest


p5 : Int -> Int -> Int
p5 from to =
    -- Smallest multiple
    let
        isValid i =
            Utils.all (\v -> Utils.divisibleBy v i) (List.range from to)

        helper n =
            if isValid n then
                n

            else
                helper (n + 1)
    in
    helper 1


p6 : Int -> Int -> Int
p6 from to =
    --  Sum square difference
    let
        list =
            List.range from to
    in
    (List.sum list ^ 2) - List.sum (List.map (\v -> v ^ 2) list)


p7 : Int -> Int
p7 n =
    --  10001st prime
    Prime.firstPrimesReversed n
        |> List.head
        |> Maybe.withDefault -1


p8 : Int -> Int
p8 numberOfDigits =
    -- Largest product in a series
    let
        digits =
            """
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450
"""
                |> String.toList
                |> List.filter Char.isDigit
                |> List.map Convert.charToInt
    in
    List.foldl
        -- Integer -> (Integer, List Integer) -> (Integer, List Integer)
        (\digit ( bestProductSoFar, previousDigits ) ->
            if List.length (List.take numberOfDigits previousDigits) == numberOfDigits then
                ( max bestProductSoFar (List.product (List.take numberOfDigits previousDigits))
                , digit :: previousDigits
                )

            else
                ( bestProductSoFar, digit :: previousDigits )
        )
        ( -1, [] )
        digits
        |> Tuple.first


p9 : () -> Int
p9 _ =
    -- Special Pythagorean triplet
    -- a + b + c = 1000 and a^2 + b^2 = c^2 then what is a*b*c ?
    let
        isTriplet a b =
            toFloat a + toFloat b + sqrt (toFloat (a ^ 2 + b ^ 2)) == 1000
    in
    List.range 1 1000
        |> List.map
            (\a ->
                List.range (a + 1) 1000
                    |> List.filter (isTriplet a)
                    |> List.map (\b -> ( a, b, 1000 - a - b ))
            )
        |> List.concat
        |> List.head
        |> Maybe.map (Debug.log "")
        |> Maybe.map (\( a, b, c ) -> a * b * c)
        |> Maybe.withDefault -1


p10 : Int -> Int
p10 top =
    -- Summation of primes
    Prime.primesUnder top
        |> List.sum


p11 : Int -> Int
p11 numberOfAdjacentNumbers =
    -- Largest product in a grid
    let
        input =
            """
    08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08
    49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00
    81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65
    52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91
    22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80
    24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50
    32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70
    67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21
    24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72
    21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95
    78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92
    16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57
    86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58
    19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40
    04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66
    88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69
    04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36
    20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16
    20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54
    01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48
    """
                |> Convert.numberGrid

        height =
            Array.length input

        width =
            input
                |> Array.get 0
                |> Maybe.map Array.length
                |> Maybe.withDefault 0
    in
    Utils.cartesianProduct
        (Utils.range2 ( 0, width ) ( 0, height ))
        [ ( 0, 1 ), ( 1, 0 ), ( 1, 1 ), ( 1, -1 ) ]
        |> List.map
            (\( ( w, h ), ( x, y ) ) ->
                List.range 0 (numberOfAdjacentNumbers - 1)
                    |> List.map
                        (\n ->
                            Array.get (h + n * y) input
                                |> Maybe.andThen (Array.get (w + n * x))
                        )
                    |> MaybeExtra.values
            )
        |> List.filter (\list -> List.length list == numberOfAdjacentNumbers)
        |> List.map List.product
        |> List.maximum
        |> Maybe.withDefault -2


p12 : Int -> Int
p12 numberOfDivisors =
    -- Highly divisible triangular number
    let
        test triangle =
            List.length (Prime.factors triangle) >= numberOfDivisors
    in
    TriangleNumber.findFirstTriangleNumberFor test


p13 : () -> String
p13 _ =
    -- Large sum
    let
        input =
            """
37107287533902102798797998220837590246510135740250
46376937677490009712648124896970078050417018260538
74324986199524741059474233309513058123726617309629
91942213363574161572522430563301811072406154908250
23067588207539346171171980310421047513778063246676
89261670696623633820136378418383684178734361726757
28112879812849979408065481931592621691275889832738
44274228917432520321923589422876796487670272189318
47451445736001306439091167216856844588711603153276
70386486105843025439939619828917593665686757934951
62176457141856560629502157223196586755079324193331
64906352462741904929101432445813822663347944758178
92575867718337217661963751590579239728245598838407
58203565325359399008402633568948830189458628227828
80181199384826282014278194139940567587151170094390
35398664372827112653829987240784473053190104293586
86515506006295864861532075273371959191420517255829
71693888707715466499115593487603532921714970056938
54370070576826684624621495650076471787294438377604
53282654108756828443191190634694037855217779295145
36123272525000296071075082563815656710885258350721
45876576172410976447339110607218265236877223636045
17423706905851860660448207621209813287860733969412
81142660418086830619328460811191061556940512689692
51934325451728388641918047049293215058642563049483
62467221648435076201727918039944693004732956340691
15732444386908125794514089057706229429197107928209
55037687525678773091862540744969844508330393682126
18336384825330154686196124348767681297534375946515
80386287592878490201521685554828717201219257766954
78182833757993103614740356856449095527097864797581
16726320100436897842553539920931837441497806860984
48403098129077791799088218795327364475675590848030
87086987551392711854517078544161852424320693150332
59959406895756536782107074926966537676326235447210
69793950679652694742597709739166693763042633987085
41052684708299085211399427365734116182760315001271
65378607361501080857009149939512557028198746004375
35829035317434717326932123578154982629742552737307
94953759765105305946966067683156574377167401875275
88902802571733229619176668713819931811048770190271
25267680276078003013678680992525463401061632866526
36270218540497705585629946580636237993140746255962
24074486908231174977792365466257246923322810917141
91430288197103288597806669760892938638285025333403
34413065578016127815921815005561868836468420090470
23053081172816430487623791969842487255036638784583
11487696932154902810424020138335124462181441773470
63783299490636259666498587618221225225512486764533
67720186971698544312419572409913959008952310058822
95548255300263520781532296796249481641953868218774
76085327132285723110424803456124867697064507995236
37774242535411291684276865538926205024910326572967
23701913275725675285653248258265463092207058596522
29798860272258331913126375147341994889534765745501
18495701454879288984856827726077713721403798879715
38298203783031473527721580348144513491373226651381
34829543829199918180278916522431027392251122869539
40957953066405232632538044100059654939159879593635
29746152185502371307642255121183693803580388584903
41698116222072977186158236678424689157993532961922
62467957194401269043877107275048102390895523597457
23189706772547915061505504953922979530901129967519
86188088225875314529584099251203829009407770775672
11306739708304724483816533873502340845647058077308
82959174767140363198008187129011875491310547126581
97623331044818386269515456334926366572897563400500
42846280183517070527831839425882145521227251250327
55121603546981200581762165212827652751691296897789
32238195734329339946437501907836945765883352399886
75506164965184775180738168837861091527357929701337
62177842752192623401942399639168044983993173312731
32924185707147349566916674687634660915035914677504
99518671430235219628894890102423325116913619626622
73267460800591547471830798392868535206946944540724
76841822524674417161514036427982273348055556214818
97142617910342598647204516893989422179826088076852
87783646182799346313767754307809363333018982642090
10848802521674670883215120185883543223812876952786
71329612474782464538636993009049310363619763878039
62184073572399794223406235393808339651327408011116
66627891981488087797941876876144230030984490851411
60661826293682836764744779239180335110989069790714
85786944089552990653640447425576083659976645795096
66024396409905389607120198219976047599490197230297
64913982680032973156037120041377903785566085089252
16730939319872750275468906903707539413042652315011
94809377245048795150954100921645863754710598436791
78639167021187492431995700641917969777599028300699
15368713711936614952811305876380278410754449733078
40789923115535562561142322423255033685442488917353
44889911501440648020369068063960672322193204149535
41503128880339536053299340368006977710650566631954
81234880673210146739058568557934581403627822703280
82616570773948327592232845941706525094512325230608
22918802058777319719839450180888072429661980811197
77158542502016545090413245809786882778948721859617
72107838435069186155435662884062257473692284509516
20849603980134001723930671666823555245252804609722
53503534226472524250874054075591789781264330331690
            """
                |> Convert.numberColumn

        digits =
            List.sum input
                |> String.fromInt
                |> String.left 11
    in
    String.left 1 digits ++ String.dropLeft 2 digits


p14 : Int -> Int
p14 top =
    -- Longest Collatz sequence
    let
        length : Int -> Int -> Int
        length starter count =
            if starter == 1 then
                1 + count

            else if remainderBy 2 starter == 0 then
                length (starter // 2) (count + 1)

            else if starter > 300000000 then
                lengthBig (BigInt.add (BigInt.mul (BigInt.fromInt 3) (BigInt.fromInt starter)) (BigInt.fromInt 1)) (count + 1)

            else
                length (3 * starter + 1) (count + 1)

        lengthBig : BigInt -> Int -> Int
        lengthBig starter count =
            if BigInt.compare starter (BigInt.fromInt 1) == EQ then
                1 + count

            else if BigInt.isEven starter then
                lengthBig (BigInt.div starter (BigInt.fromInt 2)) (count + 1)

            else
                lengthBig (BigInt.add (BigInt.mul (BigInt.fromInt 3) starter) (BigInt.fromInt 1)) (count + 1)
    in
    List.range 1 top
        |> List.map (\next -> ( next, length next 0 ))
        |> List.foldl
            (\( next, nextLength ) ( starter, longest ) ->
                if nextLength > longest then
                    ( next, nextLength )

                else
                    ( starter, longest )
            )
            ( 1, 1 )
        |> Tuple.first


p15 : ( Int, Int ) -> Int
p15 ( width, length ) =
    -- Lattice paths
    let
        -- this is not efficient enough for (20,20)
        -- but I got the results for (2,2) (3,3) (4,4) etc
        -- 6,20,70,252,924,3432
        -- https://oeis.org/A000984 -> just take the 20th number there
        recurse ( w, l ) =
            if w == width && l == length then
                1

            else if w == width then
                recurse ( w, l + 1 )

            else if l == length then
                recurse ( w + 1, l )

            else
                recurse ( w + 1, l ) + recurse ( w, l + 1 )
    in
    recurse ( 0, 0 )


p16 : Int -> Int
p16 power =
    -- Power digit sum
    BigInt.pow (BigInt.fromInt 2) (BigInt.fromInt power)
        |> BigInt.toString
        |> Convert.allDigits
        |> List.sum


p17 : () -> Int
p17 _ =
    -- Number letter counts
    let
        sum : List String -> Int
        sum =
            List.map String.length >> List.sum

        from1to9 =
            sum [ "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" ]

        from10to19 =
            sum [ "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" ]

        multiplesOf10 =
            sum [ "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" ]

        hundred =
            String.length "hundred"

        and =
            String.length "and"

        oneThousand =
            String.length "one thousand" - 1

        from1to19 =
            from1to9 + from10to19

        from20to99 =
            (10 * multiplesOf10) + (8 * from1to9)

        from100to999 =
            (900 * hundred)
                + (891 * and)
                + (100 * from1to9)
                + (9 * (from1to19 + from20to99))
    in
    from1to19 + from20to99 + from100to999 + oneThousand


p18input : String
p18input =
    """75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
"""


p18 : String -> Int
p18 input =
    -- Number letter count
    let
        piramid : Array (Array Int)
        piramid =
            Convert.numberGrid input

        totalRows : Int
        totalRows =
            Array.get (Array.length piramid - 1) piramid
                |> Maybe.map Array.length
                |> Utils.orCrash "empty piramid"

        connections : Int -> List Int
        connections index =
            List.filter (\i -> i >= 0) [ index, index + 1 ]

        stepDown : Int -> List ( Int, Int ) -> List ( Int, Int )
        stepDown row list =
            List.concatMap
                (\( index, value ) ->
                    connections index
                        |> List.map (stepDown1 row value)
                )
                list
                |> keepBest

        stepDown1 : Int -> Int -> Int -> ( Int, Int )
        stepDown1 row value newIndex =
            ( newIndex
            , value
                + (Array.get row piramid
                    |> Utils.orCrash "row out of bounds"
                    |> Array.get newIndex
                    |> Utils.orCrash "index out of bounds"
                  )
            )

        -- this is for p67
        keepBest : List ( Int, Int ) -> List ( Int, Int )
        keepBest list =
            List.foldl
                (\( index, value ) dict ->
                    Dict.update index
                        (\maybe ->
                            case maybe of
                                Nothing ->
                                    Just value

                                Just other ->
                                    Just (max other value)
                        )
                        dict
                )
                Dict.empty
                list
                |> Dict.toList
    in
    List.range 0 (totalRows - 1)
        |> List.foldl stepDown [ ( -1, 0 ) ]
        |> ExtsList.maximumBy Tuple.second
        |> Utils.orCrash "empty maximum"
        |> Tuple.second


p19 : () -> Int
p19 _ =
    -- Counting Sundays
    Utils.cartesianProduct
        (List.range 1901 2000)
        [ Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec ]
        |> List.filterMap (\( year, month ) -> Calendar.getWeekday year month 1)
        |> List.filter ((==) Sun)
        |> List.length


p20 : Int -> Int
p20 input =
    -- Factorial digit sum
    let
        factorial : BigInt -> BigInt
        factorial n =
            if BigInt.compare n (BigInt.fromInt 0) == EQ then
                BigInt.fromInt 1

            else
                BigInt.mul n (factorial (BigInt.sub n (BigInt.fromInt 1)))
    in
    factorial (BigInt.fromInt input)
        |> BigInt.toString
        |> Convert.allDigits
        |> List.sum


p21 : () -> Int
p21 _ =
    -- Amicable numbers
    let
        isAmicable : Int -> Bool
        isAmicable n =
            n /= d n && n == d (d n)

        d : Int -> Int
        d n =
            Prime.factors n
                |> List.filter ((/=) n)
                |> List.sum
    in
    List.range 1 10000
        |> List.filter isAmicable
        |> List.sum


p22 : String -> Int
p22 input =
    -- Names scores
    let
        parser : Parser (List String)
        parser =
            Parser.succeed identity
                |. Parser.spaces
                |= Parser.loop [] parserLoop
                |. Parser.spaces

        parserLoop : List String -> Parser (Parser.Step (List String) (List String))
        parserLoop revNames =
            Parser.oneOf
                [ Parser.succeed (\name -> Parser.Loop (name :: revNames))
                    |. Parser.symbol "\""
                    |= Parser.getChompedString (Parser.chompUntil "\"")
                    |. Parser.oneOf [ Parser.symbol "\",", Parser.symbol "\"" ]
                , Parser.succeed ()
                    |> Parser.map (\_ -> Parser.Done (List.reverse revNames))
                ]

        alphabeticalScore : String -> Int
        alphabeticalScore name =
            String.toList name
                |> List.map Char.toCode
                |> List.map (\code -> code - 64)
                |> List.sum
    in
    case Parser.run parser input of
        Err e ->
            Debug.todo (Debug.toString e)

        Ok list ->
            list
                |> List.sort
                |> List.foldl
                    (\name ( i, score ) ->
                        -- let _ = Debug.log name (i * alphabeticalScore name) in
                        ( i + 1, score + (i * alphabeticalScore name) )
                    )
                    ( 1, 0 )
                |> Tuple.second


p23 : () -> Int
p23 _ =
    -- Non-abundant sums
    let
        limit : Int
        limit =
            -- 30
            28123

        abundantNumbers : List Int
        abundantNumbers =
            List.range 1 limit
                |> List.filter isAbundant

        isAbundant : Int -> Bool
        isAbundant n =
            (Prime.factors n
                |> List.filter ((/=) n)
                |> List.sum
            )
                > n

        nonAbundantSum : Int -> Bool
        nonAbundantSum n =
            abundantNumbers
                |> List.filter (\a -> a < n)
                |> isSumOfTwoElems n
                |> not

        isSumOfTwoElems : Int -> List Int -> Bool
        isSumOfTwoElems target candidates =
            Utils.cartesianAny (\a b -> a + b == target) candidates candidates
    in
    List.range 1 limit
        |> List.filter nonAbundantSum
        -- |> Debug.log ""
        |> List.sum


p24 : Int -> List Int -> String
p24 nth digits =
    -- Lexicographic permutations
    let
        initialS =
            { moving = True
            , head = []
            , tail = []
            , skip = 0
            }

        next : Int -> List Int -> List Int
        next skipAtStart list =
            -- let _ = Debug.log "" (skipAtStart, list) in
            if skipAtStart > List.length list then
                list

            else
                let
                    afterBubble =
                        bubble { initialS | skip = skipAtStart } list
                in
                if afterBubble.moving then
                    next (skipAtStart + 1) list

                else
                    afterBubble.head
                        ++ List.sort afterBubble.tail

        bubble =
            List.foldr
                (\d s ->
                    -- let _ = Debug.log "foldr" ( d, s ) in
                    if not s.moving then
                        { s | head = d :: s.head }

                    else if s.skip > 0 then
                        { s | skip = s.skip - 1, tail = d :: s.tail }

                    else
                        case bubbleTail d s of
                            ( Nothing, _ ) ->
                                { s | tail = d :: s.tail }

                            ( Just _, b ) ->
                                { b | tail = d :: b.tail }
                )

        bubbleTail d s =
            List.foldl
                (\tailDigit ( candidateBubbler, b ) ->
                    if tailDigit > d then
                        case candidateBubbler of
                            Nothing ->
                                ( Just tailDigit
                                , { b
                                    | moving = False
                                    , head = [ tailDigit ]
                                  }
                                )

                            Just candidate ->
                                if tailDigit < candidate then
                                    ( Just tailDigit
                                    , { b
                                        | head = [ tailDigit ]
                                        , tail = candidate :: b.tail
                                      }
                                    )

                                else
                                    ( Just candidate
                                    , { b | tail = tailDigit :: b.tail }
                                    )

                    else
                        ( candidateBubbler
                        , { b | tail = tailDigit :: b.tail }
                        )
                )
                ( Nothing, { s | tail = [] } )
                s.tail

        helper : Int -> List Int -> List Int
        helper i list =
            if i == nth then
                list

            else
                helper (i + 1) (next 0 list)
    in
    helper 1 (List.sort digits)
        |> List.map String.fromInt
        |> String.join ""


p25 : Int -> Int
p25 digits =
    -- 1000-digit Fibonacci number
    Fibonacci.firstFit (\d -> String.length (BigInt.toString d) >= digits)
        |> Tuple.first


p26 : Int -> Int
p26 limit =
    -- Reciprocal cycles
    let
        indexOf : Int -> Int -> List Int -> Maybe Int
        indexOf index needle haystack =
            case haystack of
                [] ->
                    Nothing

                x :: xs ->
                    if x == needle then
                        Just index

                    else
                        indexOf (index + 1) needle xs

        isRepeating : List Int -> Maybe Int
        isRepeating nominators =
            case nominators of
                [] ->
                    Nothing

                last :: previous ->
                    indexOf 0 last previous

        helper : List Int -> Int -> Int -> Int
        helper previousNominators denominator nominator =
            case isRepeating previousNominators of
                Just length ->
                    length

                Nothing ->
                    let
                        remainder =
                            remainderBy denominator nominator
                    in
                    helper (nominator :: previousNominators) denominator (remainder * 10)

        cycleLength : Int -> Int
        cycleLength n =
            helper [] n 10
    in
    List.range 1 limit
        |> List.foldl
            (\nextNumber ( longest, lengthToBeat ) ->
                let
                    nextLength =
                        cycleLength nextNumber
                in
                if nextLength > lengthToBeat then
                    ( nextNumber, nextLength )

                else
                    ( longest, lengthToBeat )
            )
            ( 0, 0 )
        |> Tuple.first


p27 : Int -> Int -> Int
p27 maxA maxB =
    -- Quadratic primes
    let
        getPrimeCount c a b =
            if Prime.isPrime (c ^ 2 + a * c + b) then
                getPrimeCount (c + 1) a b

            else
                c
    in
    Utils.cartesianProduct
        (List.range -maxA maxA)
        (List.range -maxB maxB |> List.filter Prime.isPrime)
        |> List.foldl
            (\( a, b ) ( product, bestCount ) ->
                let
                    count =
                        getPrimeCount 0 a b
                in
                if count > bestCount then
                    ( a * b, count )

                else
                    ( product, bestCount )
            )
            ( 0, 0 )
        |> Tuple.first


p28 : Int -> Maybe Int
p28 width =
    -- Number spiral diagonals
    let
        helper : Int -> Int -> Int -> Int
        helper sum currentDigit currentWidth =
            if currentWidth == width then
                sum

            else
                let
                    increment =
                        currentWidth + 1

                    sumOfArm =
                        (currentDigit + increment)
                            + (currentDigit + (2 * increment))
                            + (currentDigit + (3 * increment))
                            + (currentDigit + (4 * increment))
                in
                helper (sum + sumOfArm) (currentDigit + (4 * increment)) (currentWidth + 2)
    in
    if remainderBy 2 width /= 1 then
        Nothing

    else
        Just (helper 1 1 1)


p29 : Int -> Int -> Int
p29 maxA maxB =
    -- Distinct powers
    Utils.cartesianProduct
        (List.range 2 maxA)
        (List.range 2 maxB)
        |> List.map (\( a, b ) -> BigInt.pow (BigInt.fromInt a) (BigInt.fromInt b))
        |> List.sortWith BigInt.compare
        |> List.foldl
            (\p ( last, count ) ->
                if BigInt.compare p last == EQ then
                    ( p, count )

                else
                    ( p, count + 1 )
            )
            ( BigInt.fromInt 0, 0 )
        |> Tuple.second


p30 : Int -> Int
p30 power =
    --Digit fifth powers
    List.range 2 200000
        |> List.filter
            (\i ->
                String.fromInt i
                    |> Convert.allDigits
                    |> List.map (\d -> d ^ power)
                    |> List.sum
                    |> (==) i
            )
        |> List.sum

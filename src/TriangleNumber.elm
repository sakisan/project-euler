module TriangleNumber exposing (..)


triangleNumber : Int -> Int
triangleNumber n =
    List.sum (List.range 0 n)


findFirstTriangleNumberFor : (Int -> Bool) -> Int
findFirstTriangleNumberFor predicate =
    let
        test n triangle =
            if predicate triangle then
                triangle

            else
                test (n + 1) (n + 1 + triangle)
    in
    test 1 1


isTriangleNumber : Int -> Bool
isTriangleNumber value =
    -- value == findFirstTriangleNumberFor (\i -> i >= value)
    -- p = n(n+1)/2
    -- 2p = n^2 + n
    -- 2p < n^2
    value == triangleNumber (floor (sqrt (toFloat (2 * value))))


pentagonalNumber : Int -> Int
pentagonalNumber n =
    (n * (3 * n - 1)) // 2


isPentagonal : Int -> Bool
isPentagonal i =
    -- p = (n * (3 * n - 1)) // 2
    -- 2 * p = n * (3 * n - 1)
    -- 2p = 3n^2 - n
    -- 2p/3 = n^2 - n/3
    -- 2p/3 > n^2
    -- sqrt(2p/3) > n
    i == pentagonalNumber (ceiling (sqrt (toFloat i * 2 / 3)))


hexagonalNumber : Int -> Int
hexagonalNumber n =
    n * (2 * n - 1)


isHexagonal : Int -> Bool
isHexagonal i =
    -- p = n * (2 * n - 1)
    -- p = 2n^2 - n
    -- p/2 = n^2 - n/2
    -- p/2 > n^2
    i == hexagonalNumber (ceiling (sqrt (toFloat i / 2)))

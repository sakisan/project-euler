module Palindrome exposing (isIntPalindrome, isPalindrome)


isPalindrome : String -> Bool
isPalindrome input =
    String.fromList (List.reverse (String.toList input)) == input


isIntPalindrome : Int -> Bool
isIntPalindrome =
    String.fromInt >> isPalindrome

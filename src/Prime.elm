module Prime exposing (..)

import Utils


factors : Int -> List Int
factors num =
    let
        firstHalf =
            List.range 1 (floor (sqrt (toFloat num)))
                |> List.filter (\v -> remainderBy v num == 0)
    in
    firstHalf
        |> List.foldl
            (\factor list ->
                if factor * factor == num then
                    list

                else
                    (num // factor) :: list
            )
            firstHalf
        |> List.sort


primeFactorsSet : Int -> List Int
primeFactorsSet num =
    List.range 2 (num // 2)
        |> List.filter (\v -> remainderBy v num == 0)
        |> List.filter isPrime


primeFactorsAndExponents : Int -> List ( Int, Int )
primeFactorsAndExponents num =
    let
        exponent c p n =
            if remainderBy p n == 0 then
                exponent (c + 1) p (n // p)

            else
                c
    in
    primeFactorsSet num
        |> List.map (\prime -> ( exponent 0 prime num, prime ))


primeFactors : Int -> List Int
primeFactors num =
    primeFactorsAndExponents num
        |> List.map (\( c, p ) -> List.repeat c p)
        |> List.concat


isPrime : Int -> Bool
isPrime num =
    (num > 0)
        && (List.range 2 (floor (sqrt (toFloat num)))
                |> List.filter (\v -> remainderBy v num == 0)
                |> List.length
           )
        == 0


isComposite : Int -> Bool
isComposite =
    not << isPrime


firstPrimes : Int -> List Int
firstPrimes num =
    firstPrimesNaive [ 2 ] 2 num


firstPrimesReversed : Int -> List Int
firstPrimesReversed num =
    firstPrimesNaive [ 2 ] 2 num


firstPrimesNaive : List Int -> Int -> Int -> List Int
firstPrimesNaive list i toGo =
    if toGo == 0 then
        list

    else if isPrime i then
        firstPrimesNaive (i :: list) (i + 1) (toGo - 1)

    else
        firstPrimesNaive list (i + 1) toGo


isPrimeImproved : List Int -> Int -> Bool
isPrimeImproved relevantPrimes n =
    Utils.all (\prime -> n == prime || remainderBy prime n /= 0)
        relevantPrimes


primesUnder : Int -> List Int
primesUnder top =
    let
        filter : Int -> List Int -> List Int -> List Int
        filter prime primes remainingNumbers =
            let
                filteredForThisPrime =
                    List.filter (\n -> remainderBy prime n /= 0) remainingNumbers
            in
            case Utils.first (\n -> n > prime) filteredForThisPrime of
                Nothing ->
                    prime :: primes

                Just nextPrime ->
                    filter nextPrime (prime :: primes) filteredForThisPrime

        relevantPrimes : List Int
        relevantPrimes =
            filter 2 [] (List.range 2 (floor (sqrt (toFloat top))))
    in
    List.range 2 top
        |> List.filter (isPrimeImproved relevantPrimes)


incrementalPrimeList : List Int -> Int -> List Int
incrementalPrimeList primes top =
    case primes of
        [] ->
            List.reverse (primesUnder top)

        highest :: _ ->
            (List.range highest top
                |> List.filter (isPrimeImproved primes)
                |> List.reverse
            )
                ++ primes

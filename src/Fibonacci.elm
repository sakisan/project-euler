module Fibonacci exposing (firstFit, sequence)

import BigInt exposing (BigInt)


sequence : (Int -> Bool) -> List Int
sequence until =
    if until 1 then
        1
            :: (if until 2 then
                    2 :: helper until 1 2

                else
                    []
               )

    else
        []


helper : (Int -> Bool) -> Int -> Int -> List Int
helper until a b =
    let
        next =
            a + b
    in
    if until next then
        next :: helper until b next

    else
        []


firstFit : (BigInt -> Bool) -> ( Int, BigInt )
firstFit test =
    if test (BigInt.fromInt 1) then
        ( 1, BigInt.fromInt 1 )

    else if test (BigInt.fromInt 2) then
        ( 3, BigInt.fromInt 2 )

    else
        bigHelper test 4 (BigInt.fromInt 1) (BigInt.fromInt 2)


bigHelper : (BigInt -> Bool) -> Int -> BigInt -> BigInt -> ( Int, BigInt )
bigHelper test index a b =
    let
        next =
            BigInt.add a b
    in
    if test next then
        ( index, next )

    else
        bigHelper test (index + 1) b next

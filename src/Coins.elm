module Coins exposing (numberOfCombinations)


numberOfCombinations : List Int -> Int -> Int
numberOfCombinations coins value =
    let
        helper : List Int -> List Int -> Int -> Int
        helper coinsUsed coinsLeft accumulated =
            if accumulated > value then
                0

            else if accumulated == value then
                -- Debug.log (Debug.toString coinsUsed)
                1

            else
                case coinsLeft of
                    [] ->
                        0

                    coin :: more ->
                        let
                            newUsed =
                                case coinsUsed of
                                    [] ->
                                        [ 1 ]

                                    u :: us ->
                                        (u + 1) :: us

                            usingCoin =
                                helper
                                    newUsed
                                    coinsLeft
                                    (accumulated + coin)

                            notUsingCoin =
                                helper
                                    (0 :: coinsUsed)
                                    more
                                    accumulated
                        in
                        usingCoin + notUsingCoin
    in
    helper [] coins 0

module Utils exposing (..)

import Bitwise exposing (shiftLeftBy, shiftRightBy)


divisibleBy : Int -> Int -> Bool
divisibleBy small big =
    remainderBy small big == 0


isEven : Int -> Bool
isEven n =
    divisibleBy 2 n


isOdd : Int -> Bool
isOdd =
    not << isEven


isSquare : Int -> Bool
isSquare num =
    let
        s =
            intsqrt num
    in
    (s * s) == num


all : (a -> Bool) -> List a -> Bool
all validator list =
    list
        |> List.map validator
        |> List.all identity


first : (a -> Bool) -> List a -> Maybe a
first predicate list =
    case list of
        [] ->
            Nothing

        x :: more ->
            if predicate x then
                Just x

            else
                first predicate more


range2 : ( Int, Int ) -> ( Int, Int ) -> List ( Int, Int )
range2 ( width1, width2 ) ( height1, height2 ) =
    cartesianProduct
        (List.range width1 width2)
        (List.range height1 height2)


cartesianProduct : List a -> List b -> List ( a, b )
cartesianProduct listA listB =
    List.map (\a -> List.map (\b -> ( a, b )) listB) listA
        |> List.concat


cartesianAny : (a -> b -> Bool) -> List a -> List b -> Bool
cartesianAny test listA listB =
    List.any (\a -> List.any (\b -> test a b) listB) listA


orCrash : String -> Maybe a -> a
orCrash reason maybe =
    case maybe of
        Nothing ->
            Debug.todo reason

        Just value ->
            value



{- https://discourse.elm-lang.org/t/how-to-compute-square-roots-without-dependency-on-float-arithmetics/4324/6 -}


intsqrt : Int -> Int
intsqrt n =
    intsqrtHelper (rdcrHelper n 0x40000000) n 0


rdcrHelper : Int -> Int -> Int
rdcrHelper n v =
    if v > n then
        rdcrHelper n (shiftRightBy 2 v)

    else
        v


intsqrtHelper : Int -> Int -> Int -> Int
intsqrtHelper plc rmndr root =
    if plc <= 0 then
        root

    else if rmndr - root - plc < 0 then
        intsqrtHelper (shiftRightBy 2 plc) rmndr (shiftRightBy 1 root)

    else
        intsqrtHelper (shiftRightBy 2 plc) (rmndr - root - plc) (shiftRightBy 1 root + plc)
